# Advanced Scientific Programming
APS Project 2018-2019 Deny - Skutnik

## Project objectives

Project instructions from [dubrayn.github.io](https://dubrayn.github.io/#2):

#### Write and use a 2D finite difference solver for the time-dependent non-relativistic Schrödinger equation.
* **Write the solver** in a mix of `Python2` and `C++11` using `Armadillo`
* Visualize the time-evolution of a wave packet with `Paraview`
* Get some **real-time monitoring information** of a run (in `Python2`)
* Implement an **automatic restart mechanism** (in `Python2`)
* Store the results in a **mongoDB database** (in `Python2`)
* Calculate some **special physical cases** (diffraction, tunneling effect, HO solutions, etc...)
* Use `git`, `doxygen`, `make`, `cxxtest`, `swig`, `remark.js`, `Paraview`

## Project Results

The requirements were met, as seen in paraview:

_Figure 1: Wave packet at the experiment's beginning_
![image1](https://gitlab.com/spoutnik/psa_desk/raw/master/animations/ftcs/image-001.jpeg?inline=false)

_Figure 2: Wave packet going through youg's slits_
![image2](https://gitlab.com/spoutnik/psa_desk/raw/master/animations/young_slits/image-006.jpeg?inline=false)

## Usage

Make once at the project's root, then use the files in `src`:

```
usage: main.py [-h] [-x X] [-y Y] -s SIZE -n STEPS [-c SLICES] -dt T -ds S
               [-pt file.png] [-is {gauss,custom}] [-m {ctcs,btcs,ftcs}] [-b]
```

Arguments detail:

|Parameter|Action|
|-|-|
|`-h`, `--help`|Show a help message and exit|
|`-x X`|The x coordinates of the field's origin|
|`-y Y`|The y coordinates of the field's origin|
|`-s SIZE`, `--size SIZE`|The field's size|
|`-n STEPS`|The number of steps to process|
|`-c SLICES`, `--slices SLICES`|The rate of exports|
|`-ds S`, `--delta_s S`|The spatial delta|
|`-pt file.png`, `--potential_file file.png`| A png to extract the potential field from|
|`-is {gauss,custom}`, `--initial-state {gauss,custom}`| The initial state's format|
|`-m {ctcs,btcs,ftcs}`, `--method {ctcs,btcs,ftcs}`| The finite difference method to use|
|`-b`, `--bar`|Use a progress bar (Requires `progress`)|
|`--save`| Save the progress to recover it later in the associated database|

## Dependencies

- `pillow`
- `pyevtk.hl`
- `progress` (optional)

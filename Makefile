default: prepare dox

prepare: 
	cd src/ ; $(MAKE) -f Makefile

dox:
	doxygen doc/Doxyfile


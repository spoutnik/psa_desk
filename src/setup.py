from setuptools import setup, Extension

module1 = Extension('_compute',
        include_dirs = ['./include/armanpy/'],
        libraries = ['armadillo'],
        sources = ['compute.i', 'state.cpp'],
        swig_opts = ["-c++", "-Wall","-I.","-I./include/armanpy/"])

setup (name = 'compute',
        py_modules = ['compute'],
        version = '1.0',
        description = 'package de calcul d etat',
        ext_modules = [module1])

#!/usr/bin/env python2

## Custom parser for PSA project
#
# This files defines a tailor-made argument parser for the project. Automatic help and default settings are available through this standard python object.

import argparse

parser = argparse.ArgumentParser(description = "PSA Project - Deny Skutnik")

parser.add_argument('-x',
        type = float,
        help = 'The x coordinates of the field\'s origin',
        default = 0,
        dest = 'x')

parser.add_argument('-y',
        type = float,
        help = 'The y coordinates of the field\'s origin',
        default = 0,
        dest = 'y')

parser.add_argument('-s',
        '--size',
        dest = 'size',
        type = int,
        help = 'The field\'s size',
        required = True)

parser.add_argument('-n',
        dest = 'steps',
        type = int,
        help = 'The number of steps to process',
        required = True)

parser.add_argument('-c',
        '--slices',
        dest = 'slices',
        type = int,
        default = 0,
        help = 'The rate of exports')

parser.add_argument('-dt',
        '--delta_t',
        type = float,
        help = 'The time delta',
        required = True,
        metavar = 'T',
        dest = 'dt')

parser.add_argument('-ds',
        '--delta_s',
        type = float,
        help = 'The spatial delta',
        required = True,
        metavar = 'S',
        dest = 'ds')

parser.add_argument('-pt',
        '--potential_file',
        type = argparse.FileType('r'),
        metavar = 'file.png',
        dest = 'pt',
        help = 'A png to extract the potential field from')

parser.add_argument('-is',
        '--initial-state',
        choices = ['gauss', 'custom'],
        default = 'gauss',
        dest = 'initial',
        help = 'The initial state\'s format')

parser.add_argument('-m',
        '--method',
        choices = ['ctcs', 'btcs', 'ftcs'],
        default = 'ctcs',
        dest = 'method',
        help = 'The finite difference method to use')

parser.add_argument('-b',
        '--bar',
        dest = 'bar',
        action = 'store_true',
        help = 'Use a progress bar')

parser.add_argument('--save',
        dest = 'save',
        action = 'store_true',
        help = 'Enable recovery support via MongoDB')

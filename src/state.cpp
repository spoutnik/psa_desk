#include "state.h"

#define hbar (6.582119514e-22)
#define mass (104.3961e-16)
#define cxi arma::cx_double(0.0, 1.0)

State::State() {
}

void State::calc_shift() {
	shift_operator = arma::cx_mat(container.n_rows, container.n_cols, arma::fill::zeros);
	for (arma::uword i = 0; i < shift_operator.n_cols - 1; i++) {
		shift_operator(i+1, i) = 1;
		shift_operator(i, i+1) = 1;
	}
}

void State::set_params(arma::cx_mat input, double in_dx, double in_dy, double in_dt, arma::cx_mat in_pot) {
	// Base pour le padding de matrices
	arma::cx_mat base = arma::cx_mat(input.n_rows + 2, input.n_cols + 2, arma::fill::zeros);

	base.submat(1,1, input.n_rows, input.n_cols) = input;
	container = arma::cx_mat(base);

	base.submat(1,1, input.n_rows, input.n_cols) = in_pot;
	potentials = arma::cx_mat(base);

	dx = in_dx;
	dy = in_dy;
	dt = in_dt;

	calc_shift();
}

void State::ftcs() {
	arma::cx_mat p1 = (cxi*hbar*dt)*(shift_operator*container)/(2*mass*dx*dx);
	arma::cx_mat p2 = (cxi*hbar*dt)*(container*shift_operator)/(2*mass*dy*dy);
	arma::cx_mat p3 = (dt/(hbar*cxi))*(potentials + hbar*cxi/dt + hbar*hbar/(mass*dx*dx) + hbar*hbar/(mass*dy*dy))%container;

	arma::cx_mat sum = p1 + p2 + p3;
	int ogsize = sum.n_rows - 2;
	container.submat(1,1, ogsize, ogsize) = sum.submat(1,1, ogsize, ogsize);
}

void State::btcs() {
	arma::cx_mat prev, next = container;
	double norm = 1;

	while (norm > 10e-15) {
		prev = next;

		arma::cx_mat p1 = -1.0*hbar*hbar*(shift_operator*prev)/(2*mass*dx*dx);
		arma::cx_mat p2 = -1.0*hbar*hbar*(prev*shift_operator)/(2*mass*dy*dy);
		arma::cx_mat p3 = cxi*hbar*container/dt;

		arma::cx_mat fact = (cxi*hbar/dt - potentials - hbar*hbar/(mass*dx*dx) - hbar*hbar/(mass*dy*dy));

		next = (p1 +p2 +p3)/fact;

		norm = State::norm(prev - next);
	}

	container = next;
}

void State::ctcs() {
	arma::cx_mat prev, next = container;
	double norm = 1;

	while (norm > 10e-15) {
		prev = next;

		arma::cx_mat p1 = -1.0*hbar*hbar*(shift_operator*prev + shift_operator*container)/(2*mass*dx*dx);
		arma::cx_mat p2 = -1.0*hbar*hbar*(prev*shift_operator + container*shift_operator)/(2*mass*dy*dy);
		arma::cx_mat p3 = (2.0*cxi*hbar/dt + potentials + hbar*hbar/(mass*dx*dx) + hbar*hbar/(mass*dy*dy))%container;

		arma::cx_mat fact = (2.0*cxi*hbar/dt - potentials - hbar*hbar/(mass*dx*dx) - hbar*hbar/(mass*dy*dy));

		next = (p1 +p2 +p3)/fact;

		norm = State::norm(prev - next);
	}

	container = next;
}

arma::cx_mat State::matrix(void) {
	arma::cx_mat out = container.submat(1, 1, container.n_rows-2, container.n_cols-2);
	return(out);
}

double State::norm(arma::cx_mat input) {
	arma::mat mr = arma::real(input), mi = arma::imag(input);
	//return arma::real(arma::accu(arma::conj(this->matrix())%this->matrix()));
	return arma::accu(mr%mr - mi%mi);
}

arma::mat State::module(void) {
	arma::mat mr = arma::real(matrix()), mi = arma::imag(matrix());
	arma::mat out = arma::sqrt(mr%mr + mi%mi);
	return out;
}

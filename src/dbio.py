#!/usr/bin/env python2

import pymongo
import datetime
import pickle
import os

def connect():
    _username = raw_input('Username: ')
    _password = raw_input('Password: ')
    _host = raw_input('Host: ')
    _dbname = raw_input('DB name: ')
    return pymongo.MongoClient('mongodb://%s:%s@%s/%s' % (_username, _password, _host, _dbname))

def setup(client, field):
    data = load_job(client, hash(field))

    if not data:
        begin_job(client, field)
        return (False, 0, [])
    else:
        res = get(client, field)[0]
        return (True, res["time"], pickle.loads(res["data"]))

## Job launching routine
#
# This will insert the object passed as an argument in the database,
# along with meta-data on the job, in a collection called `joblist`
#
# The stored documents adhere to the following format:
# { owner : owner uid, 
#   launch_date : job creation date in UTC, 
#   intitial_env : A pickled field (object defined in field_gen.py), 
#   hash : the initial env's hash, used for faster lookup }
#
# This also creates a collection dedicated to this job
# and sets an index on it for later use
#
def begin_job(client, initial_field):
    field_hash = hash(initial_field)

    job = {"owner": os.getuid(),
            "launch_date": datetime.datetime.utcnow(),
            "initial_env": pickle.dumps(initial_field),
            "hash": field_hash }
    client.group1.joblist.insert_one(job)

    collection_name = 'job_%s' % field_hash
    client.group1[collection_name].create_index('time')

    return job

## Job loading routine
#
# This will extract information on a job matching the hash 
# passed as an argument
#
def load_job(client, jobhash):
    entry = client.group1.joblist.find_one({'hash': jobhash})
    if entry:
        return True
    return False

## Insert a step into the database
#
# Uses the field passed as an argument to determine where to 
# store the data
# The documents stored adhere to the following format:
# { data: a pickled array,
#   time: the time index of the data }
#
def post(client, field, t, array):
    step = {"data": pickle.dumps(array),
            "time": t}

    collection_name = 'job_%s' % hash(field)
    client.group1[collection_name].insert_one(step)

    return step

## Recover a step from the database
#
# Uses the field passed as an argument to determine where to
# fetch the data
# If no time is set the last inserted array is fetched
#
def get(client, field, t = -1):
    collection_name = 'job_%s' % hash(field)

    if t == -1:
        res = client.group1[collection_name].find().sort("time", pymongo.DESCENDING).limit(1)
    else:
        res = client.group1[collection_name].find_one({"time": time})

    return res


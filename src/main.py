#!/usr/bin/python2

import field_gen
import compute
import dbio
import customparser
import numpy as np

args = customparser.parser.parse_args()

try:
    from progress.bar import IncrementalBar
except:
    args.bar = False

from pyevtk.hl import gridToVTK

step = 0
max_step = args.steps
slices = args.slices if args.slices != 0 else max_step/100

# Creation de l'objet field contenant l'environnement
f = field_gen.field(args.x, args.y, args.size, args.ds, args.dt)

if args.initial:
    mean_x, mean_y, covar = f.calc_center(args)
    f.calc_initial_gauss_impulse(mean_x, mean_y, covar, 1/(5*args.ds), 0)

# Setup of the database handle
if args.save:
    client = dbio.connect()
    recovered, recovered_step, recovered_state = dbio.setup(client, f)

# Check if the setup recovered an anterior job
if args.save and recovered:
    step = recovered_step
    initial_state = recovered_state
    print "Recovered job#%s" % hash(f)
else:
    step = 0
    initial_state = f._initial_state

if args.pt:
    f.potential_from_png(args.pt)

#np.set_printoptions(threshold = np.inf)
#np.set_printoptions(linewidth = np.inf)
#print(f._potential)

# Creation de l'objet state contenant les matrices calculees
st = compute.State()
st.set_params(initial_state, f._spatial_step, f._spatial_step, f._time_step, f._potential)

# Choix de la methode a executer suivant la valeur de l'argument
if args.method == 'ftcs':
    step_method = st.ftcs
elif args.method == 'btcs':
    step_method = st.btcs
elif args.method == 'ctcs':
    step_method = st.ctcs

x = np.linspace(args.x, args.size - 1, args.size , dtype=np.float64)
y = np.linspace(args.y, args.size - 1, args.size , dtype=np.float64)
z = np.linspace(0.0, 1.0, 1, dtype=np.float)

# Bar setup -- only if available
bar = IncrementalBar("Ongoing simulation", max=max_step, index=step, suffix='%(percent)d%%') if args.bar else None

while step != max_step:
    if step%slices == 0:
        bar.next(slices) if args.bar else None
        gridToVTK("./results/step_" + str(step).zfill(6), 
                x, y, z,
                pointData = {
                    'fcts.mod': (10*st.module()).reshape((args.size,args.size,1), order = 'F')
                    }
                )
        dbio.post(client, f, step, st.matrix()) if args.save else None
    step_method()
    step += 1

# Bar termination -- needed to re-enable terminal cursor
bar.finish() if args.bar else None

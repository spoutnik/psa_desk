#!/usr/bin/env python2

from PIL import Image
import numpy
import pickle

class field:
    ## Field initializer
    # 
    # Initialise the field with a size, a spatial step, a time step and a coordinate of the start of initial state
    # Also defines without a value the grid's potential and it's initial state
    def __init__(self, x, y, size, spatial_step, time_step):
        self._start_x = x
        self._start_y = y
        self._size = size
        self._spatial_step = spatial_step
        self._time_step = time_step
        self._potential = numpy.asfortranarray([[0 for _ in range(self._size)] for _ in range(self._size)], dtype=numpy.complex)
        self._initial_state = numpy.asfortranarray([[0 for _ in range(self._size)] for _ in range(self._size)], dtype=numpy.complex)

    ## Potential setter
    # 
    #  Sets the grid's potential to array's values
    def set_potential(self, array):
        if array.shape == (self._size, self._size):
            self._potential = array
            return True
        return False

    ## Initial state setter
    # 
    #  Sets the grid's initial state to array's values
    def set_initial_state(self, array):
        if array.shape == (self._size, self._size):
            self._initial_state = array
            return True
        return False

    ## Initial state calculator
    #
    # Calculates the initial state's values and sets them to the initial state's grid
    # Calculated with the function argument
    def calc_initial_state(self, function):
        for i in range (self._size):
            for j in range (self._size):
                iconv = self._start_x + self._spatial_step * i
                jconv = self._start_y + self._spatial_step * j
                self._initial_state[i][j] = function(iconv, jconv)
    
    ## Compute the field's center
    #
    # Prepare the arguments i order to automate the centering of the gaussian
    def calc_center(self, args):
        center = args.size*args.ds/4
        return (center+args.x, center*2+args.y, args.ds*5)

    ## Gauss initial state calculator
    #
    # Calculates the initial state's values and sets them to the initial state's grid
    # Calculated with the Gauss function
    def calc_initial_gauss(self, mean_x, mean_y, covar):
        for i in range (self._size):
            for j in range (self._size):
                iconv = self._start_x + self._spatial_step * i
                jconv = self._start_y + self._spatial_step * j
                X = float(iconv - mean_x)
                Y = float(jconv - mean_y)
                self._initial_state[i][j] = numpy.exp(-1 * (X**2 + Y**2)/covar**2)

    ## Gauss with impulsion initial state calculator
    #
    # Calculates the initial state's values and sets them to the initial state's grid
    # Calculated with the Gauss function
    def calc_initial_gauss_impulse(self, mean_x, mean_y, covar, k_x, k_y):
        for i in range (self._size):
            for j in range (self._size):
                iconv = self._start_x + self._spatial_step * i
                jconv = self._start_y + self._spatial_step * j
                X = float(iconv - mean_x)
                Y = float(jconv - mean_y)
                Z = numpy.complex(0,k_x * iconv + k_y * jconv)
                self._initial_state[i][j] = numpy.exp(-1*(X**2 + Y**2)/covar**2 + Z)

    ## Image potential setter
    # 
    #  Sets the grid's potential to corresponding pixel colors from png indicated by the path argument    
    def potential_from_png(self, path):
        im = Image.open(path)

        if (im.size[0] != self._size or im.size[1] != self._size):
            print("WARNING image and field are of different sizes"
                    + str(im.size[0]) 
                    + "x"
                    + str(im.size[1]) 
                    + " / " 
                    + str(self._size)
                    + "x"
                    + str(self._size))

        # Get image data and format it to fit the field size (eg _size*_size)
        pic = numpy.asfortranarray(numpy.array(im.getdata(), dtype=numpy.complex).reshape(self._size, self._size, 3)[:,:,0])
        return self.set_potential(pic)

    ## Image potential setter
    #
    # Determine a field's hash depending on its content
    def __hash__(self):
        return hash(hash(pickle.dumps(self._potential))
                * hash(pickle.dumps(self._initial_state))
                * hash(self._size)
                * hash(self._spatial_step)
                * hash(self._time_step))


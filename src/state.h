#ifndef CSSH
#define CSSH

#include <armadillo>

/*! \class State
 * \brief The simulation's state */

class State {
	private:
		arma::cx_mat container, potentials, shift_operator;
		double dx, dy, dt;

		/*! \brief Calculate the shift operator 
		 *
		 * Stored in `shift_operator`, this matrix allows to efficiently calculate the sum of values of neighbiuring indexes. */
		void calc_shift();

		/*! \brief Calculate the norm of the matrix passed as an argument */
		double norm(arma::cx_mat input);

	public:
		/*! \brief Default constructor 
		 *
		 * It takes no parameters as setting them this way caused distasteful crashes and obscur errors */
		State();

		/*! \brief Experiment parameters setup
		 *
		 * \param inp	The initial state of the wave
		 * \param dx	The space differential on the x axis
		 * \param dy	The space differential on the y axis
		 * \param dt	The time differential
		 * \param pot	The potential field of the experiment
		 *
		 * This method requires all experiment's parameters, and arranges them in memory. */
		void set_params(arma::cx_mat inp, double dx, double dy, double dt, arma::cx_mat pot);

		/*! \brief Get the matrix at the current state
		 *
		 * This method outputs the calculated data in a user-friendly format, trimming any excess internal formatting. */
		arma::cx_mat matrix();

		/*! \brief Get the norm of the current state's matrix */
		double norm() {
			return norm(matrix());
		}

		/*! \brief Get the module of the current state's matrix */
		arma::mat module();

		/*! \brief Perform a step of the FTCS finite difference method */
		void ftcs();

		/*! \brief Perform a step of the BTCS finite difference method */
		void btcs();

		/*! \brief Perform a step of the CTCS finite difference method */
		void ctcs();
};

#endif
